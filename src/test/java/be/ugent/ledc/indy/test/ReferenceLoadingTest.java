package be.ugent.ledc.indy.test;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.indy.algorithms.verification.InMemoryReferenceManager;
import be.ugent.ledc.indy.datastructures.IND;
import be.ugent.ledc.indy.datastructures.INDException;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.io.CPFParser;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;

public class ReferenceLoadingTest
{   
    private static ContractedDataset referenceData;
    
    @BeforeClass
    public static void init()
    {
        referenceData = new ContractedDataset(new Contract
            .ContractBuilder()
            .addContractor("id", TypeContractorFactory.STRING)
            .addContractor("alias", TypeContractorFactory.STRING)
            .build()
        );
        
        referenceData.addDataObject(
            new DataObject()
                .setString("id", "205")
                .setString("alias", "Jimmy")
        );
        
        referenceData.addDataObject(
            new DataObject()
                .setString("id", "205")
                .setString("alias", "Jim")
        );
        
        referenceData.addDataObject(
            new DataObject()
                .setString("id", "206")
                .setString("alias", "John")
        );
        
        referenceData.addDataObject(
            new DataObject()
                .setString("id", "206")
                .setString("alias", "Jack")
        );
    }
    
    @Test
    public void unconditionalLoadCacheTest1() throws INDException
    {
        InMemoryReferenceManager imrl = new InMemoryReferenceManager(referenceData);
        
        DataObject o = new DataObject()
            .setString("lid", "205")
            .setString("local_alias", "Jones");
        
        Map<String, SigmaContractor<?>> leftContractors = new HashMap<>();
        leftContractors.put("local_alias", SigmaContractorFactory.STRING);
        
        TreeMap<String,String> iMap = new TreeMap<>();
        iMap.put("local_alias", "alias");
        IND ind = new IND(iMap, leftContractors);
        
        assertFalse(imrl.isCached(o, ind));
    }
    
    @Test
    public void unconditionalLoadCacheTest2() throws INDException
    {
        InMemoryReferenceManager imrl = new InMemoryReferenceManager(referenceData);
        
        DataObject o = new DataObject()
            .setString("lid", "205")
            .setString("local_alias", "Jones");

        TreeMap<String,String> iMap = new TreeMap<>();
        iMap.put("local_alias", "alias");
        
        Map<String, SigmaContractor<?>> leftContractors = new HashMap<>();
        leftContractors.put("local_alias", SigmaContractorFactory.STRING);
        
        IND ind = new IND(iMap, leftContractors);
        
        imrl.getReferenceData(o, ind);
        
        assertTrue(imrl.isCached(o, ind));
    }
    
    @Test
    public void unconditionalLoadCacheTest3() throws INDException
    {
        InMemoryReferenceManager imrl = new InMemoryReferenceManager(
            referenceData,  
            1); //Single entry cache
        
        DataObject o1 = new DataObject()
            .setString("lid", "205")
            .setString("local_alias", "Jones");
        
        DataObject o2 = new DataObject()
            .setString("lid", "206")
            .setString("local_alias", "Jack");

        TreeMap<String,String> iMap = new TreeMap<>();
        iMap.put("local_alias", "alias");
        Map<String, SigmaContractor<?>> leftContractors = new HashMap<>();
        leftContractors.put("local_alias", SigmaContractorFactory.STRING);
        
        IND ind = new IND(iMap, leftContractors);
        
        imrl.getReferenceData(o1, ind);
        
        //In the unconditional case: caching is always true after the first step
        assertTrue(imrl.isCached(o2, ind));
    }
    
    @Test
    public void conditionalLoadCacheTest3() throws INDException, ParseException
    {
        InMemoryReferenceManager imrl = new InMemoryReferenceManager(referenceData);
        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("id", SigmaContractorFactory.STRING);
        contractors.put("lid", SigmaContractorFactory.STRING);
        
        TreeMap<String,String> iMap = new TreeMap<>();
        iMap.put("local_alias", "alias");
        Map<String, SigmaContractor<?>> leftContractors = new HashMap<>();
        leftContractors.put("local_alias", SigmaContractorFactory.STRING);
        
        IND ind = new IND(
            iMap,
            leftContractors,
            CPFParser.parseCPF("id == lid", contractors)
        );
        
        DataObject o1 = new DataObject()
            .setString("lid", "205")
            .setString("local_alias", "Jones");
        
        DataObject o2 = new DataObject()
            .setString("lid", "206")
            .setString("local_alias", "Jack");

        imrl.getReferenceData(o1, ind);
        
        assertTrue(imrl.isCached(o1, ind));
        assertFalse(imrl.isCached(o2, ind));
    }
    
    @Test
    public void conditionalLoadReferenceTest1() throws INDException, ParseException
    {
        InMemoryReferenceManager imrl = new InMemoryReferenceManager(referenceData);
        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("id", SigmaContractorFactory.STRING);
        contractors.put("lid", SigmaContractorFactory.STRING);
        
        TreeMap<String,String> iMap = new TreeMap<>();
        iMap.put("local_alias", "alias");
        Map<String, SigmaContractor<?>> leftContractors = new HashMap<>();
        leftContractors.put("local_alias", SigmaContractorFactory.STRING);
        IND ind = new IND(
            iMap,
            leftContractors,
            CPFParser.parseCPF("id == lid", contractors)
        );
        
        DataObject o = new DataObject()
            .setString("lid", "205")
            .setString("local_alias", "Jones");

        Set<DataObject> lrd = imrl
            .getReferenceData(o, ind)
            .stream()
            .collect(Collectors.toSet());
        
        assertTrue(lrd.size() == 2);
        assertTrue(lrd
            .stream()
            .anyMatch(ro-> ro
                    .getString("alias")
                    .equals("Jim"))
        );
        assertTrue(lrd
            .stream()
            .anyMatch(ro-> ro
                    .getString("alias")
                    .equals("Jimmy"))
        );
        
    }
    
    @Test
    public void conditionalLoadReferenceTest2() throws INDException, ParseException
    {
        InMemoryReferenceManager imrl = new InMemoryReferenceManager(referenceData);
        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("id", SigmaContractorFactory.STRING);
        contractors.put("lid", SigmaContractorFactory.STRING);
        
        TreeMap<String,String> iMap = new TreeMap<>();
        iMap.put("local_alias", "alias");
        Map<String, SigmaContractor<?>> leftContractors = new HashMap<>();
        leftContractors.put("local_alias", SigmaContractorFactory.STRING);
        IND ind = new IND(
            iMap,
            leftContractors,
            CPFParser.parseCPF("id == lid", contractors)
        );
        
        DataObject o = new DataObject()
            .setString("lid", "204")
            .setString("local_alias", "Jack");

        Set<DataObject> lrd = imrl
            .getReferenceData(o, ind)
            .stream()
            .collect(Collectors.toSet());
        
        assertTrue(lrd.isEmpty());
    }
}
