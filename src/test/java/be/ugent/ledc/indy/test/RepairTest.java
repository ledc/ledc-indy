package be.ugent.ledc.indy.test;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.indy.algorithms.verification.InMemoryReferenceManager;
import be.ugent.ledc.indy.algorithms.verification.ReferenceRegistry;
import be.ugent.ledc.indy.datastructures.IND;
import be.ugent.ledc.indy.datastructures.INDException;
import be.ugent.ledc.indy.datastructures.INDRuleset;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.io.CPFParser;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.cost.EditDistanceCostFunction;
import be.ugent.ledc.indy.algorithms.repair.IndyRepair;
import be.ugent.ledc.sigma.repair.cost.functions.IterableCostFunctionWrapper;
import be.ugent.ledc.sigma.repair.cost.models.IterableCostModel;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class RepairTest
{
     private static ContractedDataset referenceData;
    
    @BeforeClass
    public static void init()
    {
        referenceData = new ContractedDataset(new Contract
            .ContractBuilder()
            .addContractor("id", TypeContractorFactory.STRING)
            .addContractor("alias", TypeContractorFactory.STRING)
            .build()
        );
        
        referenceData.addDataObject(
            new DataObject()
                .setString("id", "205")
                .setString("alias", "Jimmy")
        );
        
        referenceData.addDataObject(
            new DataObject()
                .setString("id", "205")
                .setString("alias", "Jim")
        );
        
        referenceData.addDataObject(
            new DataObject()
                .setString("id", "206")
                .setString("alias", "John")
        );
        
        referenceData.addDataObject(
            new DataObject()
                .setString("id", "206")
                .setString("alias", "Jack")
        );
    }
    
    @Test
    public void conditionalIndRepairTest1() throws INDException, RepairException, ParseException
    {
        InMemoryReferenceManager imrc = new InMemoryReferenceManager(referenceData);
        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("id", SigmaContractorFactory.STRING);
        contractors.put("lid", SigmaContractorFactory.STRING);
        
        TreeMap<String,String> iMap = new TreeMap<>();
        iMap.put("local_alias", "alias");
        Map<String, SigmaContractor<?>> leftContractors = new HashMap<>();
        leftContractors.put("local_alias", SigmaContractorFactory.STRING);
        
        IND ind = new IND(
            iMap,
            leftContractors,
            CPFParser.parseCPF("id == lid", contractors)
        );
        
        INDRuleset ruleset = new INDRuleset(ind);
        
        
        ReferenceRegistry.getInstance().register(ind, imrc);
        
        ContractedDataset dataset = new ContractedDataset(new Contract
            .ContractBuilder()
            .addContractor("lid", SigmaContractorFactory.STRING)
            .addContractor("local_alias", SigmaContractorFactory.STRING)
            .build()
        );
                
        dataset.addDataObject(new DataObject()
            .setString("lid", "206")
            .setString("local_alias", "Jones"));
        
        IterableCostModel costModel = new IterableCostModel();
        
        costModel.addCostFunction(
            "local_alias",
            new IterableCostFunctionWrapper(new EditDistanceCostFunction())
        );

        Dataset repair = new IndyRepair(ruleset, costModel).repair(dataset);
        
        
        Assert.assertTrue(repair.getSize() == 1);
        Assert.assertTrue(repair.getDataObjects().contains(new DataObject()
            .setString("lid", "206")
            .setString("local_alias", "John")));
         
    }
}
