package be.ugent.ledc.indy.test;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.indy.algorithms.verification.InMemoryReferenceManager;
import be.ugent.ledc.indy.algorithms.verification.ReferenceRegistry;
import be.ugent.ledc.indy.datastructures.IND;
import be.ugent.ledc.indy.datastructures.INDException;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.io.CPFParser;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;

public class DetectionTest
{
    private static ContractedDataset referenceData;
    
    @BeforeClass
    public static void init()
    {
        referenceData = new ContractedDataset(new Contract
            .ContractBuilder()
            .addContractor("id", TypeContractorFactory.STRING)
            .addContractor("alias", TypeContractorFactory.STRING)
            .build()
        );
        
        referenceData.addDataObject(
            new DataObject()
                .setString("id", "205")
                .setString("alias", "Jimmy")
        );
        
        referenceData.addDataObject(
            new DataObject()
                .setString("id", "205")
                .setString("alias", "Jim")
        );
        
        referenceData.addDataObject(
            new DataObject()
                .setString("id", "206")
                .setString("alias", "John")
        );
        
        referenceData.addDataObject(
            new DataObject()
                .setString("id", "206")
                .setString("alias", "Jack")
        );
    }
    
    @Test
    public void conditionalIndDetectionTest1() throws INDException, ParseException
    {
        InMemoryReferenceManager imrc = new InMemoryReferenceManager(referenceData);
        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("id", SigmaContractorFactory.STRING);
        contractors.put("lid", SigmaContractorFactory.STRING);
        
        Map<String, SigmaContractor<?>> leftContractors = new HashMap<>();
        leftContractors.put("local_alias", SigmaContractorFactory.STRING);
        
        TreeMap<String,String> iMap = new TreeMap<>();
        iMap.put("local_alias", "alias");
        
        IND ind = new IND(
            iMap,
            leftContractors,
            CPFParser.parseCPF("id == lid", contractors)
        );
        
        
        DataObject o1 = new DataObject()
            .setString("lid", "206")
            .setString("local_alias", "Jack");
        
        DataObject o2 = new DataObject()
            .setString("lid", "206")
            .setString("local_alias", "Jones");
        
        ReferenceRegistry.getInstance().register(ind, imrc);
        
        assertTrue(imrc.isSatisfied(o1, ind));
        assertTrue(ind.isSatisfied(o1));
        assertFalse(imrc.isSatisfied(o2, ind));
        assertFalse(ind.isSatisfied(o2));
    }
}
