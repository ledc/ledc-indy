package be.ugent.ledc.indy.datastructures;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import java.util.function.Predicate;

public class INDRuleset extends RuleSet<DataObject, IND>
{

    public INDRuleset(Set<IND> rules) {
        super(rules);
    }

    public INDRuleset(IND... rules) {
        super(rules);
    }
    
    public INDRuleset leftProject(Set<String> leftAttributes)
    {
        return new INDRuleset(
            stream()
            .filter(ind -> leftAttributes.containsAll(ind.getAttributesToTest()))
            .collect(Collectors.toSet()));
    }
    
    public INDRuleset rightProject(Set<String> rightAttributes)
    {
        return 
            new INDRuleset(
            stream()
            .filter(ind -> rightAttributes.containsAll(ind.getReferenceAttributes()))
            .collect(Collectors.toSet()));
    }

    public Map<String, SigmaContractor<?>> getLeftContractors()
    {
        return stream()
            .flatMap(ind -> ind
                .getLeftContractorMap()
                .entrySet()
                .stream())
            .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
    }

    public INDRuleset filter(Predicate<IND> test)
    {
        return new INDRuleset(
            stream()
            .filter(ind -> test.test(ind))
            .collect(Collectors.toSet())
        );
    }
}
