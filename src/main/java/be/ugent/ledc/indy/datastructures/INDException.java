package be.ugent.ledc.indy.datastructures;

import be.ugent.ledc.core.LedcException;

public class INDException extends LedcException
{
    public INDException(String message)
    {
        super(message);
    }

    public INDException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public INDException(Throwable cause)
    {
        super(cause);
    }

    public INDException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
