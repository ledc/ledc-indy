package be.ugent.ledc.indy.datastructures;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.datastructures.rules.Rule;
import be.ugent.ledc.indy.algorithms.verification.ReferenceRegistry;
import be.ugent.ledc.indy.io.INDParser;
import be.ugent.ledc.sigma.datastructures.atoms.AbstractAtom;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.formulas.CPF;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A class that models an INclusion Dependency by means of a mapping of attributes 
 * from a right dataset to a left dataset.
 * @author abronsel
 */
public class IND implements Rule<DataObject>
{    
    /**
     * Encodes the mapping from attributes in the object to test to objects in 
     * the reference data.
     */
    private final TreeMap<String,String> inclusionMapping;
       
    /**
     * A condition on the reference data that needs to be satisfied.
     */
    private final CPF condition;
    
    private final Map<String, SigmaContractor<?>> leftContractorMap;
    
    public IND(TreeMap<String, String> inclusionMapping, Map<String, SigmaContractor<?>> leftContractorMap, CPF condition) throws INDException
    {
        this.inclusionMapping = inclusionMapping;
        this.leftContractorMap = leftContractorMap;
        this.condition = condition;
    }
    
    public IND(TreeMap<String, String> inclusionMapping, Map<String, SigmaContractor<?>> leftContractorMap) throws INDException
    {
        this(inclusionMapping, leftContractorMap, new CPF(AbstractAtom.ALWAYS_TRUE));
    }
    
    public Set<String> getAttributesToTest()
    {
        return new HashSet<>(inclusionMapping.keySet());
    }
    
    public Set<String> getReferenceAttributes()
    {
        return new HashSet<>(inclusionMapping.values());
    }
    
    public String getReferredAttribute(String leftAttribute)
    {
        return this.inclusionMapping.get(leftAttribute);
    }

    public CPF getCondition()
    {
        return condition;
    }

    
    public SigmaContractor<?> getContractor(String attribute)
    {
        return condition.getAttributes().contains(attribute)
            ? condition
                .getAtoms()
                .stream()
                .filter(atom -> atom
                    .getAttributes()
                    .anyMatch(aa -> aa.equals(attribute)))
                .findFirst()
                .get()
                .getContractor()
            : null;
    }
    
    public Set<String> projection()
    {
        return Stream.concat(
            this
                .getAttributesToTest()
                .stream(),
            this
                .getCondition()
                .getVariableAtoms()
                .stream()
                .map(va -> va.getRightAttribute()))
        .collect(Collectors.toSet());
    }

    public Map<String, SigmaContractor<?>> getLeftContractorMap()
    {
        return leftContractorMap;
    }
    
    public boolean isUnary()
    {
        return inclusionMapping.size() == 1;
    }
    
    public boolean hasConstantCondition()
    {
        return condition == null || !condition.hasVariableAtoms();
    }
    
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 19 * hash + Objects.hashCode(this.inclusionMapping);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final IND other = (IND) obj;
        if (!Objects.equals(this.inclusionMapping, other.inclusionMapping))
        {
            return false;
        }
        return true;
    }

    public TreeMap<String, String> getInclusionMapping()
    {
        return inclusionMapping;
    }  

    @Override
    public String toString()
    {
        return inclusionMapping
            .keySet()
            .stream()
            .collect(Collectors.joining(",","[","]"))
            + " " + INDParser.IN + " "
            + inclusionMapping
            .keySet()
            .stream()
            .map(a -> inclusionMapping.get(a))
            .collect(Collectors.joining(",","[","]"))
            + (condition.isContradiction()
            ? ""
            : " " + INDParser.WHERE + " " + condition);
    }

    @Override
    public Set<String> getInvolvedAttributes()
    {
        return new HashSet<>(inclusionMapping.keySet());
    }

    @Override
    public boolean test(DataObject o)
    {
        try
        {
            return ReferenceRegistry.getInstance().hasReferenceManager(this)
                ? ReferenceRegistry.getInstance().getReferenceManager(this).isSatisfied(o, this)
                : false;
        }
        catch (INDException ex) {
            Logger.getLogger(IND.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    
}
