package be.ugent.ledc.indy.io;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.indy.datastructures.IND;
import be.ugent.ledc.indy.datastructures.INDException;
import be.ugent.ledc.indy.datastructures.INDRuleset;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.io.CPFParser;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class INDParser
{
    public static final String IN = "in";
    public static final String WHERE = "where";
    
    public static INDRuleset parseInclusionDependencies(List<String> lines, Map<String, SigmaContractor<?>> contractors) throws ParseException, INDException
    {
        //INDs
        Set<IND> inds = new HashSet<>();
        
        for(String line: lines)
        {
            String regex =
                "^\\[(.+)\\]\\s+"
                + IN
                + "\\s+\\[(.+)\\]"
                + "(\\s+"
                + WHERE
                + "(.+)" //Condition
                + ")?$";
            
            //Compile regex
            Pattern p = Pattern.compile(regex);
            
            //Get matcher
            Matcher m = p.matcher(line);
            
            if(m.matches())
            {
                //Extract parts
                List<String> leftAttributes = parseAttributes(m.group(1));
                List<String> rightAttributes = parseAttributes(m.group(2));
                String condition = m.group(4);

                if(leftAttributes.size() != rightAttributes.size())
                    throw new INDException("Cannot parse inclusion dependencies for line "
                        + line
                        + ".\nUnequal amount of attributes.");
                
                Map<String, SigmaContractor<?>> leftContractors = new HashMap<>();
                
                for(String lAttribute: leftAttributes)
                {
                    if(!contractors.containsKey(lAttribute))
                        throw new INDException("Cannot parse inclusion dependencies for line "
                            + line
                            + ".\nAttribute " + lAttribute + " has no contractor.");
                    
                    leftContractors.put(lAttribute, contractors.get(lAttribute));
                }
                
                TreeMap<String,String> iMap = new TreeMap<>();
                
                for(int i=0; i<leftAttributes.size(); i++)
                {
                    iMap.put(leftAttributes.get(i), rightAttributes.get(i));
                }
                
                IND ind = condition == null || condition.trim().isEmpty()
                    ? new IND(iMap, leftContractors)
                    : new IND(
                        iMap,
                        leftContractors,
                        CPFParser.parseCPF(condition, contractors)
                    );
                
                //Add
                inds.add(ind);
            }
            else
                throw new INDException("An IND must be specified in the following format: [<LOCAL_ATTRIBUTES>] in [<REFERENCE_ATTRIBUTES>] (where <condition>)?");
        }
        
        return new INDRuleset(inds);
    }
    
    private static List<String> parseAttributes(String attributes)
    {
        return Stream
            .of(attributes.trim().split("\\s*,\\s*"))
            .collect(Collectors.toList());
    }
    
    public static String format(IND ind)
    {
        return ind.toString();
    }
}
