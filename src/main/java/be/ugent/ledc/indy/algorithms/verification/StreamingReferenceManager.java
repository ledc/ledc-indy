package be.ugent.ledc.indy.algorithms.verification;

import be.ugent.ledc.core.binding.BindingException;
import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.binding.jdbc.DBMS;
import be.ugent.ledc.core.binding.jdbc.JDBCBinder;
import be.ugent.ledc.core.binding.jdbc.PreparedJDBCDataReader;
import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.indy.datastructures.IND;
import be.ugent.ledc.indy.datastructures.INDException;
import be.ugent.ledc.sigma.datastructures.atoms.AbstractAtom;
import be.ugent.ledc.sigma.datastructures.atoms.AtomType;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A reference loader where the entire reference data is assumed to reside in 
 * a JDBC binded database. Reference data is then fetched as needed.
 * @author abronsel
 */
public class StreamingReferenceManager extends AbstractReferenceManager
{
    private final JDBCBinder referenceBinder;
            
    private final String sqlToFetch;
    
    private final Map<IND, PreparedJDBCDataReader> readerMap;

    private final Contract dataContract;
    
    public StreamingReferenceManager(JDBCBinder referenceBinder, String sqlToFetch, Contract dataContract, int cacheSize)
    {
        super(cacheSize);
        this.referenceBinder = referenceBinder;
        this.sqlToFetch = sqlToFetch.endsWith(";")
            ? sqlToFetch.substring(0, sqlToFetch.length()-1).trim()
            : sqlToFetch.trim();
        this.dataContract = dataContract;
        this.readerMap = new HashMap<>();
    }

    public StreamingReferenceManager(JDBCBinder referenceBinder, String sqlToFetch, Contract dataContract)
    {
        super();
        this.referenceBinder = referenceBinder;
        this.sqlToFetch = sqlToFetch.endsWith(";")
            ? sqlToFetch.substring(0, sqlToFetch.length()-1).trim()
            : sqlToFetch.trim();
        this.dataContract = dataContract;
        this.readerMap = new HashMap<>();
    }

    @Override
    public Set<DataObject> getReferenceData(DataObject o, IND ind) throws INDException
    {
        PreparedJDBCDataReader reader = compile(ind);
        
        Set<DataObject> localReferenceData = null;
        
        if(isCached(o, ind))
        {
            localReferenceData = super.getFromCache(o, ind);
        }
        else
        {
            try
            {
                localReferenceData = reader
                    .readContractedData(o.project(getVariablePart(ind)))
                    .stream()
                    .collect(Collectors.toSet());
            }
            catch (DataReadException ex)
            {
                throw new INDException(ex);
            }
        }
        
        updateCache(o, ind, localReferenceData);
        
        return localReferenceData;
    }
    
    private PreparedJDBCDataReader compile(IND ind) throws INDException
    {
        if(readerMap.get(ind) == null)
        {
            try
            {   
                String preparedSql = "with all_data as ("
                        + sqlToFetch + ") "
                        + "select " //Build the select clause
                        + ind
                            .getReferenceAttributes()
                            .stream()
                            .map(a -> referenceBinder
                                    .getRdb()
                                    .getVendor()
                                    .getAgent()
                                    .escaping()
                                    .apply(a))
                            .collect(Collectors.joining(",")) + " "
                        + "from all_data"; //Get the data from the inline view

                //If there are no conditions, return without where clause
                if(ind.getCondition().getAtoms().isEmpty())
                {
                    readerMap.put(
                        ind,
                        new PreparedJDBCDataReader(
                            referenceBinder,
                            preparedSql,
                            new HashSet<>())
                    );
                }
                //Border case: with contradiction
                else if(ind.getCondition()
                    .getAtoms()
                    .stream()
                    .anyMatch(aa -> aa.equals(AbstractAtom.ALWAYS_FALSE))
                )
                {
                    readerMap.put(
                        ind,
                        new PreparedJDBCDataReader(
                            referenceBinder,
                            preparedSql.concat(" where false"),
                            new HashSet<>())
                    );
                }
                //In the non-expected case where there are tautologies, also return
                //with no where clause
                else if(ind.getCondition()
                    .getAtoms()
                    .stream()
                    .allMatch(aa -> aa.equals(AbstractAtom.ALWAYS_TRUE))
                )
                {
                    readerMap.put(
                        ind,
                        new PreparedJDBCDataReader(
                            referenceBinder,
                            preparedSql,
                            new HashSet<>())
                    );
                }
                else
                {
                    DBMS vendor = referenceBinder.getRdb().getVendor();

                    String cPart = ind
                        .getCondition()
                        .getAtoms()
                        .stream()
                        .filter(atom -> atom.getAtomType() != AtomType.VARIABLE)
                        .map(atom -> atom.toSqlString(vendor))
                        .collect(Collectors.joining(" and ")).trim();

                    String vPart = 
                        ind
                        .getCondition()
                        .getVariableAtoms()
                        .stream()
                        .map(va -> vendor
                                .getAgent()
                                .escaping()
                                .apply(va.getLeftAttribute())
                            + " "
                            + va.getOperator().getSqlSymbol()
                            + " "
                            + (dataContract
                                .getAttributes()
                                .contains(va.getRightAttribute())
                                ? PreparedJDBCDataReader.NAME_PREFIX
                                : "")
                            + va.getRightAttribute())
                        .collect(Collectors.joining(" and "))
                        .trim();
                        
                        Set<String> parameterNames = ind
                            .getCondition()
                            .getVariableAtoms()
                            .stream()
                            .map(va -> va.getRightAttribute())
                            .filter(ra -> dataContract
                                    .getAttributes()
                                    .contains(ra))
                            .collect(Collectors.toSet());

                    preparedSql += " where "
                        + Stream.of(vPart, cPart)
                        .filter(s -> !s.isEmpty())
                        .collect(Collectors.joining(" and ")); 

                    readerMap.put(
                        ind,
                        new PreparedJDBCDataReader(
                            referenceBinder,
                            preparedSql,
                            parameterNames
                        )
                    ); 
                }
            }       
            catch(SQLException | BindingException ex)
            {
                throw new INDException(ex);
            }
        }
        
        return readerMap.get(ind);
    }
}
