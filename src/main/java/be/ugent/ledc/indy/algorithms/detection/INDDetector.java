package be.ugent.ledc.indy.algorithms.detection;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.indy.datastructures.IND;
import be.ugent.ledc.indy.datastructures.INDException;
import be.ugent.ledc.indy.datastructures.INDRuleset;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class INDDetector
{
    public static Map<IND, Set<DataObject>> detectErrors(Dataset data, INDRuleset inds) throws INDException
    {
        //Initialize mapping of violations
        Map<IND, Set<DataObject>> violationMap = new HashMap<>();
        
        //Iterate over inds
        for(IND ind: inds)
        {

            for(DataObject o: data)
            {
                if(!ind.isSatisfied(o))
                {        
                    violationMap.merge(
                        ind,
                        SetOperations.set(o.project(ind.projection())),
                        SetOperations::union
                    );
                }
            }
        }
        
        return violationMap;
    }
}
