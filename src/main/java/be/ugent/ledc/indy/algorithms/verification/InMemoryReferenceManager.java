package be.ugent.ledc.indy.algorithms.verification;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.indy.datastructures.IND;
import be.ugent.ledc.indy.datastructures.INDException;
import be.ugent.ledc.sigma.datastructures.atoms.AbstractAtom;
import be.ugent.ledc.sigma.datastructures.atoms.AtomType;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A reference checker where the entire reference data is kept in memory.
 * @author abronsel
 */
public class InMemoryReferenceManager extends AbstractReferenceManager
{
    private final Dataset referenceData;
    
    public InMemoryReferenceManager(Dataset referenceData, int cacheSize)
    {
        super(cacheSize);
        this.referenceData = referenceData;
    }

    public InMemoryReferenceManager(Dataset referenceData)
    {
        super();
        this.referenceData = referenceData;
    }

    @Override
    public Set<DataObject> getReferenceData(DataObject o, IND ind) throws INDException
    {
        Set<DataObject> localReferenceData = null;
        
        if(isCached(o, ind))
        {
            localReferenceData = super.getFromCache(o, ind);
        }
        else
        {
            //Specify the condition
            List<AbstractAtom<?,?,?>> fixedAtoms = ind
                .getCondition()
                .getVariableAtoms()
                .stream()
                .map(va -> va.fixRight(o))
                .collect(Collectors.toList());

            //Compile a test
            Predicate test = ind.getCondition().getAtoms().isEmpty()
                ? (obj) -> true
                : Stream.concat(
                ind
                    .getCondition()
                    .getAtoms()
                    .stream()
                    .filter(aa -> aa.getAtomType() != AtomType.VARIABLE),
                fixedAtoms
                    .stream()
            )
            .map(a->(Predicate)a)
            .reduce(Predicate::and)
            .orElseThrow(() -> new INDException("Could not reduce atoms to a single predicate."));

            localReferenceData = referenceData
                .select(test)
                .project(ind.getReferenceAttributes())
                .stream()
                .collect(Collectors.toSet());
            
            if(localReferenceData.isEmpty())
                System.err.println("Warning: loaded reference data for object "
                    + o
                    + " is empty. "
                    + "The inclusion dependency will not be repairable for this object."
                );
        }
        
        updateCache(o, ind, localReferenceData);
        
        return localReferenceData.stream().collect(Collectors.toSet());
    }
}
