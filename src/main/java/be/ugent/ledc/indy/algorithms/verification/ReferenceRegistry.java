package be.ugent.ledc.indy.algorithms.verification;

import be.ugent.ledc.indy.datastructures.IND;
import java.util.HashMap;
import java.util.Map;

public class ReferenceRegistry
{
    private static final ReferenceRegistry INSTANCE = new ReferenceRegistry();
    
    private final Map<IND, ReferenceManager> managers;
    
    private ReferenceRegistry()
    {
        this.managers = new HashMap<>();
    }
    
    public static ReferenceRegistry getInstance()
    {
        return INSTANCE;
    }
    
    public void register(IND ind, ReferenceManager referenceManager)
    {
        managers.put(ind, referenceManager);
    }
    
    public ReferenceManager getReferenceManager(IND ind)
    {
        return managers.get(ind);
    }
    
    public boolean hasReferenceManager(IND ind)
    {
        return managers.containsKey(ind);
    }
}
