package be.ugent.ledc.indy.algorithms.verification;

import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.indy.datastructures.IND;
import be.ugent.ledc.indy.datastructures.INDException;
import be.ugent.ledc.sigma.datastructures.atoms.AbstractAtom;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * An interface that implements the logic to fetch reference data for an object o
 * in the scope of a specific inclusion dependency.
 * @author abronsel
 */
public interface ReferenceManager
{
    /**
     * Gets the reference data
     * @param o
     * @param ind
     * @return 
     * @throws be.ugent.ledc.indy.datastructures.INDException 
     */
    public Set<DataObject> getReferenceData(DataObject o, IND ind) throws INDException;
    
    public default boolean isSatisfied(DataObject o, IND ind) throws INDException
    {       
        return getReferenceData(o, ind).contains(translate(o,ind));
    }
    
    public default SigmaRuleset buildSigmaRuleset(DataObject o, IND ind, Contract contract) throws INDException
    {
        //Fetch reference data
        Set<DataObject> translation = getReferenceData(o, ind)
            .stream()
            .map(ro -> inverseTranslate(ro, ind))
            .collect(Collectors.toSet());
        
        //Build contractors
        Map<String, SigmaContractor<Comparable<? super Comparable>>> contractors = ind
            .getAttributesToTest()
            .stream()
            .collect(Collectors.toMap(
                a -> a,
                a -> (SigmaContractor<Comparable<? super Comparable>>)contract.getAttributeContract(a))
            );
        
        //For each attribute, get permitted values.
        Map<String, Set<Comparable<? super Comparable>>> permittedValueMap = ind
            .getAttributesToTest()
            .stream()
            .collect(Collectors.toMap(
                    a -> a,
                    a -> translation
                        .stream()
                        .filter(ro -> ro.get(a) != null)
                        .map(ro -> (Comparable<? super Comparable>)ro.get(a))
                        .collect(Collectors.toSet())
            ));
        
        //Build domain constraints
        Set<SigmaRule> sigmaRules = ind
            .getAttributesToTest()
            .stream()
            .map(a -> new SigmaRule(contractors.get(a)
                .buildSetAtom(
                    a,
                    SetOperator.NOTIN,
                    permittedValueMap.get(a)))
            )
            .collect(Collectors.toSet());
        
        //In case interactions appear, add interaction rules
        if(!ind.isUnary())
        {
            //Find the attribute with most permitted values
            int maxValues = permittedValueMap
                .values()
                .stream()
                .mapToInt(v -> v.size())
                .max()
                .getAsInt();
            
            String collapseAttribute = permittedValueMap
                .keySet()
                .stream()
                .filter(a -> permittedValueMap.get(a).size() == maxValues)
                .findFirst()
                .get();

            
            if(collapseAttribute == null)
                throw new INDException("Error in conversion to SigmaRuleset. "
                + "Cause: cannot find an attribute with maximal amount of permitted values");
            
           
            Map<DataObject, Set<Comparable<? super Comparable>>> interactionMap =
                translation
                .stream()
                .collect(Collectors.toMap(
                    ro -> ro.inverseProject(collapseAttribute),
                    ro -> SetOperations.set((Comparable<? super Comparable>)ro.get(collapseAttribute)),
                    SetOperations::union,
                    () -> new HashMap<>()));
            
            for(DataObject ko: interactionMap.keySet())
            {
                Set<AbstractAtom<?,?,?>> atoms =
                    ko
                    .getAttributes()
                    .stream()
                    .filter(a -> ko.get(a) != null)
                    .map(a -> contractors
                        .get(a)
                        .buildConstantAtom(
                            a,
                            EqualityOperator.EQ,
                            (Comparable<? super Comparable>) o.get(a)))
                    .collect(Collectors.toSet());
                
                atoms.add(contractors
                    .get(collapseAttribute)
                    .buildSetAtom(
                        collapseAttribute,
                        SetOperator.NOTIN,
                        interactionMap.get(ko)));
                
                sigmaRules.add(new SigmaRule(atoms));
            }
                    
                
        }
            
        return new SigmaRuleset(
            contractors
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
                    e -> e.getKey(),
                    e -> e.getValue()))
                ,
            sigmaRules
        );   
    }
    
    public default DataObject translate(DataObject o, IND ind)
    {
        DataObject t = new DataObject(o.isCaseSensitiveNames());
        
        for(String ta: ind.getAttributesToTest())
        {
            t.set(ind.getReferredAttribute(ta), o.get(ta));
        }
        
        return t;
    }
    
    public default DataObject inverseTranslate(DataObject reference, IND ind)
    {
        DataObject t = new DataObject(reference.isCaseSensitiveNames());
        
        for(String ta: ind.getAttributesToTest())
        {
            t.set(ta, reference.get(ind.getReferredAttribute(ta)));
        }
        
        return t;
    }
}
