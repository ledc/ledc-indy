package be.ugent.ledc.indy.algorithms.repair;

import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.indy.algorithms.verification.ReferenceRegistry;
import be.ugent.ledc.indy.datastructures.IND;
import be.ugent.ledc.indy.datastructures.INDException;
import be.ugent.ledc.indy.datastructures.INDRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SufficientSigmaRuleset;
import be.ugent.ledc.sigma.repair.LoCoRepair;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.sigma.repair.cost.models.IterableCostModel;
import be.ugent.ledc.sigma.repair.selection.RandomRepairSelection;
import be.ugent.ledc.sigma.sufficientsetgeneration.FCFWrapper;

public class IndyRepair
{
    private final INDRuleset ruleset;

    private final IterableCostModel costModel;
    
    public IndyRepair(INDRuleset ruleset, IterableCostModel costModel)
    {
        this.ruleset = ruleset;
        this.costModel = costModel;
    }
    
    public Dataset repair(ContractedDataset dataset) throws INDException, RepairException
    {
        Dataset cleanData = new SimpleDataset();
        
        for(DataObject o: dataset)
        {
            boolean clean = true;
            
            for(IND ind: ruleset)
            {
                if(ind.isFailed(o))
                {
                    clean = false;
                    break;
                }
            }
            
            cleanData.addDataObject(
                clean
                ? o
                : repair(o, dataset.getContract())
            );
        }
        
        return cleanData;
    }

    private DataObject repair(DataObject o, Contract contract) throws INDException, RepairException
    {
        SigmaRuleset mergedRuleset = null;
        
        for(IND ind: ruleset)
        {
            if(!ReferenceRegistry.getInstance().hasReferenceManager(ind))
                throw new RepairException("Cannot repair ind " + ind + ". Cause: no reference manager found");
            
            SigmaRuleset local = ReferenceRegistry
                .getInstance()
                .getReferenceManager(ind)
                .buildSigmaRuleset(o, ind, contract);
            
            mergedRuleset = mergedRuleset == null
                ? local
                : mergedRuleset.merge(local);
        }
        
        //Compose sufficient set
        SufficientSigmaRuleset sufficientSet = new FCFWrapper().build(mergedRuleset);
        
        return new LoCoRepair(
            sufficientSet,
            costModel,
            new RandomRepairSelection())
        .repair(o);
    }
    
    public INDRuleset getRuleset()
    {
        return ruleset;
    }
}
