package be.ugent.ledc.indy.algorithms.verification;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.indy.datastructures.IND;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class AbstractReferenceManager implements ReferenceManager
{
    private final int cacheSize;
    
    private final LinkedHashMap<DataObject, Set<DataObject>> cache = new LinkedHashMap<>();

    public AbstractReferenceManager(int cacheSize)
    {
        this.cacheSize = Math.max(cacheSize, 1);
    }
    
    public AbstractReferenceManager()
    {
        this(50);
    }

    public boolean isCached(DataObject o, IND ind)
    {
        Set<String> vPart = getVariablePart(ind);
        
        return cache
            .keySet()
            .stream()
            .anyMatch(co -> o.project(vPart).equals(co.project(vPart)));
    }
    
    public Set<DataObject> getFromCache(DataObject o, IND ind)
    {
        return cache.get(o.project(getVariablePart(ind)));
    }
    
    public void updateCache(DataObject o, IND ind, Set<DataObject> reference)
    {
        DataObject cacheKey = o.project(getVariablePart(ind));

        //If the cacheKey is currently there, replace it to the last position
        //Else just add it at the back
        if(cache.containsKey(cacheKey))
            cache.remove(cacheKey);
        
        cache.put(cacheKey, reference);
        
        
        //If the cache size becomes too large, remove the oldest entry
        if(cache.size() > cacheSize)
            cache
                .remove(cache
                    .keySet()
                    .stream()
                    .findFirst()
                    .get());
    }
    
    protected Set<String> getVariablePart(IND ind)
    {
        return ind
            .getCondition()
            .getVariableAtoms()
            .stream()
            .map(va -> va.getRightAttribute())
            .collect(Collectors.toSet());
    }
}
