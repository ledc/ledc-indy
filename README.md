# ledc-indy

## Why

Inclusion dependencies are, next to Functional dependencies (FDs), one of the most popular types of constraints to enforce referential integrity.
As a data quality tool, there are especially handy to verify consistency within/between databases or to test data against a reference database.


## What
This repository is part of ledc, which is short for *Lightweight Engine for Data quality Control*. 
It is a modular framework designed for data quality monitoring and improvement in several different ways.

The current repo contains ledc-indy: an extension to the [sigma](https://www.gitlab.com/ledc/ledc-sigma) repository.
More precisely, the main features of ledc-indy are

* **IND model**: a simple syntax to define (conditional) inclusion dependencies and reference managers to define the data source where reference data resides.

* **Reference managers**: efficient handling of reference data by means of prepared statements and controllable, in-memory caching of reference data.

* **Repair**: translation of sets of INDs to equivalent sets of sigma rules, which allows usage of relevant [repair algorithms](https://gitlab.com/ledc/ledc-sigma/-/blob/master/docs/repair.md) for sigma rules.
In particular, repair algorithms with non-constant cost models are useful here.


## Getting started

As all current and future components of ledc, ledc-indy is written in Java.
To run it, you require Java 17 (as from v1.2) or higher and Maven.
Just pull the code from this repository and build it with Maven.
Alternatively, you can have a look at the registry to download an executable JAR file.
